import * as React from "react";

import { graphql } from "gatsby";

import Layout from "../components/layout";
import Seo from "../components/seo";

import Home from "../components/sections/home";
import About from "../components/sections/about";
import Services from "../components/sections/services";
import Contact from "../components/sections/contact";
import Team from "../components/sections/team";

const IndexPage = () => {
   return (
      <Layout>
         <Seo title="Home" />
         <Home />
         <Services />
         <About />
         <Team />
         <Contact />
      </Layout>
   );
};

export default IndexPage;

export const query = graphql`
   query ($language: String!) {
      locales: allLocale(filter: { language: { eq: $language } }) {
         edges {
            node {
               ns
               data
               language
            }
         }
      }
   }
`;
