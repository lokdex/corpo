import * as React from "react";
import { Fragment, useState, useEffect } from "react";
import { Listbox, Popover, Transition } from "@headlessui/react";
import { useI18next, useTranslation } from "gatsby-plugin-react-i18next";

import logo from "../../images/logo.png";

import {
   ChartBarIcon,
   CursorClickIcon,
   MenuIcon,
   RefreshIcon,
   ShieldCheckIcon,
   ViewGridIcon,
   XIcon,
} from "@heroicons/react/outline";

import { Link } from "gatsby";
import { Link as LinkS } from "react-scroll";

const Navbar = () => {
   const { t } = useTranslation();
   const { language, languages, changeLanguage } = useI18next();

   const sections = [
      {
         name: t("NAVBAR:HOME"),
         to: "home",
         icon: ChartBarIcon,
      },
      {
         name: t("NAVBAR:SERVICES"),

         to: "services",
         icon: CursorClickIcon,
      },
      {
         name: t("NAVBAR:ABOUT"),
         to: "about",
         icon: ShieldCheckIcon,
      },
      {
         name: t("NAVBAR:TEAM"),
         to: "team",
         icon: ViewGridIcon,
      },
      {
         name: t("NAVBAR:CONTACT"),
         to: "contact",
         icon: RefreshIcon,
      },
   ];

   const [scrollNav, setScrollNav] = useState(false);

   const changeNav = () => {
      if (window.scrollY >= 80) {
         setScrollNav(true);
      } else {
         setScrollNav(false);
      }
   };

   useEffect(() => {
      window.addEventListener("scroll", changeNav);
   }, []);

   return (
      <Popover
         className={`sticky top-0 z-50 bg-white ${
            scrollNav ? "opacity-90" : "opacity-100"
         }   `}
      >
         <div className="max-w-7xl mx-auto px-4 sm:px-6">
            <div className="flex justify-between items-center py-6 md:justify-start md:space-x-10">
               <div className="flex justify-start lg:w-0 lg:flex-1">
                  <Link to="/">
                     <img className="w-auto sm:h-20 h-16 -my-4" src={logo} alt="" />
                  </Link>
               </div>
               <div className="-mr-2 -my-2 md:hidden">
                  <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-red-500">
                     <span className="sr-only">Open menu</span>
                     <MenuIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
               </div>
               <Popover.Group as="nav" className="hidden md:flex space-x-10">
                  {sections.map((section, index) => (
                     <LinkS
                        key={`section-${index}`}
                        to={section.to}
                        className="text-base font-medium text-gray-500 hover:text-gray-900"
                        smooth={true}
                        duration={500}
                        spy={true}
                        exact="true"
                        offset={-80}
                     >
                        {section.name}
                     </LinkS>
                  ))}
               </Popover.Group>

               <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
                  <Listbox
                     value={language}
                     onChange={(lng) => {
                        changeLanguage(lng);
                     }}
                  >
                     <div className="relative mt-1">
                        <Listbox.Button className="relative w-full py-2 pl-3 pr-5 text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-orange-300 focus-visible:ring-offset-2 focus-visible:border-red-500 sm:text-sm">
                           <span
                              className="block"
                              style={{ textTransform: "uppercase" }}
                           >
                              <span>
                                 {language} {language === "es" ? "🇪🇸" : "🇺🇸"}
                              </span>
                           </span>
                        </Listbox.Button>
                        <Transition
                           as={Fragment}
                           leave="transition ease-in duration-100"
                           leaveFrom="opacity-100"
                           leaveTo="opacity-0"
                        >
                           <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                              {languages.map((lng, index) => (
                                 <Listbox.Option
                                    key={`lng-${index}`}
                                    value={lng}
                                    style={{ textTransform: "uppercase" }}
                                    className={({ active }) =>
                                       `${
                                          active
                                             ? "text-red-900 bg-red-200"
                                             : "text-gray-900"
                                       }
                                          relative py-2 pl-4 `
                                    }
                                 >
                                    <>
                                       <span>
                                          {lng} {lng === "es" ? "🇪🇸" : "🇺🇸"}
                                       </span>
                                    </>
                                 </Listbox.Option>
                              ))}
                           </Listbox.Options>
                        </Transition>
                     </div>
                  </Listbox>
               </div>
            </div>
         </div>

         <Transition
            as={Fragment}
            enter="duration-200 ease-out"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="duration-100 ease-in"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
         >
            <Popover.Panel
               focus
               className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
            >
               <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
                  <div className="pt-5 pb-6 px-5">
                     <div className="flex items-center justify-between">
                        <div>
                           <img
                              className="h-8 w-auto"
                              src="https://tailwindui.com/img/logos/workflow-mark-red-600.svg"
                              alt="Workflow"
                           />
                        </div>
                        <div className="-mr-2">
                           <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-red-500">
                              <span className="sr-only">Close menu</span>
                              <XIcon className="h-6 w-6" aria-hidden="true" />
                           </Popover.Button>
                        </div>
                     </div>
                     <div className="mt-6">
                        <nav className="grid gap-y-8">
                           {sections.map((section, index) => (
                              <LinkS
                                 smooth={true}
                                 duration={500}
                                 spy={true}
                                 exact="true"
                                 offset={-80}
                                 key={`solution-${index}`}
                                 to={section.to}
                                 className="-m-3 p-3 flex items-center rounded-md hover:bg-gray-50"
                              >
                                 <section.icon
                                    className="flex-shrink-0 h-6 w-6 text-red-600"
                                    aria-hidden="true"
                                 />
                                 <span className="ml-3 text-base font-medium text-gray-900">
                                    {section.name}
                                 </span>
                              </LinkS>
                           ))}
                           <LinkS
                              to="contact"
                              className="text-base font-medium text-gray-500 hover:text-gray-900"
                              smooth={true}
                              duration={500}
                              spy={true}
                              exact="true"
                              offset={-80}
                           >
                              {t("NAVBAR:CONTACT")}
                           </LinkS>
                        </nav>
                     </div>
                  </div>
               </div>
            </Popover.Panel>
         </Transition>
      </Popover>
   );
};

export default Navbar;
