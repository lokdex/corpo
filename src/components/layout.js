import * as React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";

import Navbar from "./Navbar/navbar";

const Layout = ({ children }) => {
   const data = useStaticQuery(graphql`
      query SiteTitleQuery {
         site {
            siteMetadata {
               title
            }
         }
      }
   `);

   return (
      <>
         <div>
            <Navbar />
            <main className="relative">{children}</main>
            <footer
               style={{
                  marginTop: `2rem`,
               }}
            >
               © Amor Di Corpo {new Date().getFullYear()}
            </footer>
         </div>
      </>
   );
};

Layout.propTypes = {
   children: PropTypes.node.isRequired,
};

export default Layout;
