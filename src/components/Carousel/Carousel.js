import React, { Component } from "react";
import { CarouselData } from "./CarouselData";
import {
   ChevronDoubleLeftIcon,
   ChevronDoubleRightIcon,
} from "@heroicons/react/outline";
import Swipe from "react-easy-swipe";

class Carousel extends Component {
   constructor(props) {
      super(props);
      this.state = {
         currentSlide: 0,
      };
   }

   nextSlide = () => {
      let newSlide =
         this.state.currentSlide === CarouselData.length - 1
            ? 0
            : this.state.currentSlide + 1;
      this.setState({ currentSlide: newSlide });
   };

   prevSlide = () => {
      let newSlide =
         this.state.currentSlide === 0
            ? CarouselData.length - 1
            : this.state.currentSlide - 1;
      this.setState({ currentSlide: newSlide });
   };

   setCurrentSlide = (index) => {
      this.setState({ currentSlide: index });
   };

   render() {
      return (
         <div className="mt-8">
            <div className="max-w-lg h-72 flex overflow-hidden relative">
               <ChevronDoubleLeftIcon
                  onClick={this.prevSlide}
                  className="absolute left-4  inset-y-1/2 text-white cursor-pointer h-4"
               />

               <Swipe
                  onSwipeLeft={this.nextSlide}
                  onSwipeRight={this.prevSlide}
               >
                  {CarouselData.map((slide, index) => {
                     return (
                        <img
                           src={slide.image}
                           alt="This is a carousel slide"
                           key={index}
                           className={
                              index === this.state.currentSlide
                                 ? "block w-full h-auto object-cover select-none"
                                 : "hidden select-none"
                           }
                        />
                     );
                  })}
               </Swipe>

               <ChevronDoubleRightIcon
                  onClick={this.nextSlide}
                  className="absolute right-4 inset-y-1/2 text-white cursor-pointer  h-4"
               />
            </div>
         </div>
      );
   }
}

export default Carousel;
