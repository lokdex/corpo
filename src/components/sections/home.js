import * as React from "react";
import { useTranslation } from "gatsby-plugin-react-i18next";
import Carousel from "../Carousel/Carousel";

function Home() {
   const { t } = useTranslation();

   return (
      <div id="home" className="container p-5 mx-auto lg:flex justify-center">
         <main className="mt-10 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:py-20 xl:mt-0">
            <div className="sm:text-center lg:text-left">
               <h1 className="text-4xl tracking-tight font-normal text-gray-900 sm:text-5xl md:text-6xl">
                  <span className="block xl:inline font-title">
                     {`Data to enrich your `}
                  </span>
                  <span className="block text-red-300 xl:inline">
                     {t("HOME:TITLE")}
                  </span>
               </h1>
               <p className="mt-3 text-base text-gray-500 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
                  Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure
                  qui lorem cupidatat commodo. Elit sunt amet fugiat veniam
                  occaecat fugiat aliqua.
               </p>
               <div className="mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start">
                  <div className="rounded-md shadow">
                     <a
                        href="#"
                        className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-red-300 hover:bg-red-400 md:py-4 md:text-lg md:px-10"
                     >
                        Get started
                     </a>
                  </div>
               </div>
            </div>
         </main>

         <div className="flex justify-center pt-5 lg:pt-0">
            <Carousel />
         </div>
      </div>
   );
}

export default Home;
