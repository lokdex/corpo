import * as React from "react";
import { useTranslation } from "react-i18next";

function Team() {
   const { t } = useTranslation();

   const team = [
      {
         image: "https://www.thehealthyhomeeconomist.com/wp-content/uploads/2014/08/heliotherapy-benefits11.jpg",
         name: "Jhon",
         role: "Doe",
      },
      {
         image: "https://www.thehealthyhomeeconomist.com/wp-content/uploads/2014/08/heliotherapy-benefits11.jpg",
         name: "Li",
         role: "Ang",
      },
      {
         image: "https://www.thehealthyhomeeconomist.com/wp-content/uploads/2014/08/heliotherapy-benefits11.jpg",
         name: "Simoes",
         role: "Salome",
      },
      {
         image: "https://www.thehealthyhomeeconomist.com/wp-content/uploads/2014/08/heliotherapy-benefits11.jpg",
         name: "Blair",
         role: "Verona",
      },
   ];

   return (
      <div id="team" style={{ backgroundColor: "rgba(0,0,0,0.02)" }}>
         <div className="container mx-auto p-8">
            <div className="lg:text-center">
               <h1 className="text-4xl tracking-tight font-normal text-gray-900 sm:text-5xl md:text-6xl">
                  <span className="block xl:inline font-title">{`Meet our `}</span>
                  <span className="block text-red-300 xl:inline">
                     {t("TEAM:TITLE")}
                  </span>
               </h1>
            </div>
            <div className="flex flex-wrap justify-center gap-8 mt-8">
               {team.map((item, index) => (
                  <Member key={`member-${index}`} item={item} />
               ))}
            </div>
         </div>
      </div>
   );
}

function Member({ item }) {
   return (
      <div className="group flex flex-col items-center min-w-fit">
         <div className="w-32 h-32 rounded-full border-2 border-red-300 overflow-hidden -mb-16 z-10">
            <img
               className="object-cover w-full h-full group-hover:scale-110 transition-transform duration-500 ease-in-out"
               src={item.image}
            />
         </div>
         <div className="flex flex-col items-center bg-red-400/10 group-hover:bg-red-300 px-8 pb-8 pt-20 transition-colors duration-500 ease-in-out">
            <span className="text-xl text-black group-hover:text-white font-title">
               {item.name}
            </span>
            <span className="font-medium text-base text-red-300 group-hover:text-white">
               {item.role}
            </span>
            <button className="mt-2 font-medium text-sm bg-red-300 text-white group-hover:bg-white group-hover:text-black py-2 px-6">
               View Profile
            </button>
         </div>
      </div>
   );
}

export default Team;
