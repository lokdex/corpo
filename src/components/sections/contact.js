import * as React from "react";
import { useTranslation } from "react-i18next";
import {
   MailIcon,
   PhoneIcon,
   PhotographIcon,
   ArrowRightIcon,
} from "@heroicons/react/outline";
import formIcon from "../../images/formIcon.svg";
function Contact() {
   const { t } = useTranslation();

   const contactList = [
      {
         location: "Colorado",
         phone: "+1 678.536.36869",
         instagram: "@amordicorpo",
         email: "amordicorpo@gmail.com",
      },
      {
         location: "Seattle",
         phone: "+1 678.536.36869",
         instagram: "@amordicorpo",
         email: "amordicorpo@gmail.com",
      },
      {
         location: "New York",
         phone: "+1 678.536.36869",
         instagram: "@amordicorpo",
         email: "amordicorpo@gmail.com",
      },
   ];

   return (
      <div id="contact" style={styles.container}>
         <div className="lg:text-center">
            <h1 className="text-4xl tracking-tight font-normal text-gray-900 sm:text-5xl md:text-6xl">
               <span className="block text-red-300 xl:inline">
                  {t("CONTACT:TITLE")}
               </span>
               <span className="block xl:inline font-title">{` Us`}</span>
            </h1>
         </div>
         <div style={styles.itemContainer}>
            {contactList.map((contactitem) => (
               <ContactItem item={contactitem} />
            ))}
         </div>
         <div className="grid mt-16 justify-items-center">
            <ContactForm />
         </div>
      </div>
   );
}

function ContactItem({ item }) {
   return (
      <div className="relative bg-white group p-10 border-2 border-black/10 flex flex-col items-center gap-y-3.5 hover:bg-black/70 transition-colors duration-500 ease-in-out">
         <img
            src="https://html.vecuro.com/mixlax/demo/assets/img/contact/contact-3.jpg"
            className="object-cover absolute w-full h-full -m-10 -z-10"
         />
         <MailIcon className="w-16 h-16 text-red-300" />
         <span className="text-xl font-title group-hover:text-white">
            {item.location}
         </span>
         <span className="text-base group-hover:text-white">{item.phone}</span>
         <span className="text-base group-hover:text-white">
            {`Email: ${item.email}`}
         </span>
         <span className="text-base group-hover:text-white">
            Office: 14/A, Brown Tower, NewYork, US
         </span>
         <button className="px-8 py-4 text-sm font-medium bg-red-300 text-white group-hover:bg-white group-hover:text-black ">
            Get Directions
         </button>
      </div>
   );
}

function ContactForm() {
   const { t } = useTranslation();
   return (
      <div className="w-full py-16 rounded-lg shadow-lg bg-red-300/10 p-2 sm:px-20 md:px-40 lg:px-64">
         <div className="flex lg:text-center relative justify-center">
            <img src={formIcon} className="w-32 h-32 z-0 absolute -mt-10" />
            <h1 className="text-4xl tracking-tight font-normal text-gray-900 sm:text-5xl md:text-6xl relative">
               <span className="block xl:inline font-title text-center">
                  {t("CONTACT:SEND-MAIL")}
               </span>
            </h1>
         </div>

         <form className="flex flex-col mt-16 gap-8 items-center">
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4 w-full justify-items-center">
               <input
                  type="text"
                  className="form-control w-full h-12 block px-3 py-1.5 text-base font-normal bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:bg-white focus:border-red-300 focus:outline-none"
                  id="firstname"
                  placeholder="First Name"
               />
               <input
                  type="text"
                  className="form-control w-full h-12 block px-3 py-1.5 text-base font-normal  bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0  focus:bg-white focus:border-red-300 focus:outline-none"
                  id="lastname"
                  placeholder="Last Name"
               />
               <input
                  type="text"
                  className="form-control w-full h-12 block px-3 py-1.5 text-base font-normal bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:bg-white focus:border-red-300 focus:outline-none"
                  id="email"
                  placeholder="Email Address"
               />
               <input
                  type="text"
                  className="form-control w-full h-12 block px-3 py-1.5 text-base font-normal  bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0  focus:bg-white focus:border-red-300 focus:outline-none"
                  id="phone"
                  placeholder="Phone Number"
               />
            </div>
            <div className="form-group mb-6 w-full">
               <textarea
                  className="form-control block h-40 w-full px-3 py-1.5  text-base font-normal  text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-red-300 focus:outline-none"
                  id="exampleFormControlTextarea13"
                  rows="3"
                  placeholder="Message"
               />
            </div>
            <button className="flex gap-4 items-center px-8 py-4 max-w-xs text-sm font-medium bg-white text-black group-hover:bg-white">
               <div>Send email</div>
               <div className="flex bg-red-300 h-8 w-8 items-center">
                  <ArrowRightIcon className="h-4 text-white m-auto" />
               </div>
            </button>
         </form>
      </div>
   );
}

const styles = {
   container: {
      padding: "2rem",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
   },
   title: {},
   itemContainer: {
      display: "flex",
      flexWrap: "wrap",
      marginTop: "32px",
      gap: "16px",
      flexDirection: "row",
      justifyContent: "center",
   },
   item: {
      flex: 1,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      borderRadius: "0.5rem",
      margin: "1rem",
      padding: "1.5rem",
      minWidth: "15rem",
      maxWidth: "20rem",
      border: "1px solid rgba(0,0,0,0.05)",
   },
};

export default Contact;
