import * as React from "react";
import { useTranslation } from "react-i18next";
import {
   AnnotationIcon,
   BeakerIcon,
   BadgeCheckIcon,
   SparklesIcon,
   GlobeIcon,
} from "@heroicons/react/outline";

function About() {
   const { t } = useTranslation();

   const features = [
      {
         name: t("ABOUT:FIRST"),
         description: t("ABOUT:SUSTAINABLE"),
         icon: GlobeIcon,
      },
      {
         name: t("ABOUT:SECOND"),
         description: t("ABOUT:TREATMENT"),
         icon: BeakerIcon,
      },
      {
         name: t("ABOUT:THIRD"),
         description: t("ABOUT:SERVICES"),
         icon: SparklesIcon,
      },
      {
         name: t("ABOUT:FOURTH"),
         description: t("ABOUT:COMMITMENT"),
         icon: BadgeCheckIcon,
      },
   ];

   return (
      <div id="about" className="py-12 bg-white">
         <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="lg:text-center">
               <h1 className="text-4xl tracking-tight font-normal text-gray-900 sm:text-5xl md:text-6xl">
                  <span className="block xl:inline font-title">{`Find more `}</span>
                  <span className="block text-red-300 xl:inline">
                     {t("ABOUT:TITLE")}
                  </span>
               </h1>

               <p className="mt-2 text-3xl leading-8 mt-10 font-extrabold tracking-tight text-gray-900 sm:text-4xl font-title">
                  {t("ABOUT:SUBTITLE")}
               </p>
               <p className="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">
                  {t("ABOUT:DESCRIPTION")}
               </p>
            </div>

            <div className="mt-10">
               <dl className="space-y-10 md:space-y-0 md:grid md:grid-cols-1 md:gap-x-8 md:gap-y-10">
                  {features.map((feature) => (
                     <div key={feature.name} className="relative">
                        <dt>
                           <div className="absolute flex items-center self-auto justify-center h-12 w-12 rounded-md bg-red-300 text-white">
                              <feature.icon
                                 className="h-6 w-6"
                                 aria-hidden="true"
                              />
                           </div>
                           <p className="ml-16 text-lg leading-6 font-medium text-gray-900">
                              {feature.name}
                           </p>
                        </dt>
                        <dd className="mt-2 ml-16 text-base text-gray-500">
                           {feature.description}
                        </dd>
                     </div>
                  ))}
               </dl>
            </div>
         </div>
      </div>
   );
}

export default About;
