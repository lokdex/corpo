module.exports = {
   content: ["./src/**/*.{js,jsx,ts,tsx}"],
   theme: {
      extend: {
         fontFamily: {
            body: ["Commissioner"],
            title: ["Fraunces"],
         },
      },
   },
   plugins: [],
};
